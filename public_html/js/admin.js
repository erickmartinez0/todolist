$(function () {
   var APPLICATION_ID = "9656E9BF-09C8-9FA5-FF56-A8E8ABF46D00",
    SECRET_KEY = "CBF068E3-D309-DA1F-FF73-A488FF2C5D00",
    VERSION = "v1";
           
    Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);
    
    var loginScript = $("#login-template").html();
    var loginTemplate = Handlebars.compile(loginScript);
    
    $('.main-container').html(loginTemplate);
    
    $(document).on('submit', '.form-signin', function(event){
        event.preventDefault();
        
        
        var data = $(this).serializeArray(),
           email = data[0].value,
           password = data[1].value;
           
           Backendless.UserService.login(email, password, true, new Backendless.Async(userLoggedIn, gotError));
    });
});                                                                            

function Posts(args){
    args = args || {};
    this.title = args.title || "";
    this.content = args.content || "";
    this.authorEmail = args.authorEmail || "";
}

function userLoggedIn(user){
    console.log("user successfully logged in");
    
    var welcomeScript = $('#welcome-template').html();
    var welcomeTemplate = Handlebars.compile(welcomeScript);
    var welcomeHTML = welcomeTemplate(user);
    
    $('.main container').html(welcomeHTML);
}

function gotError(error){
    console.log("Error message - " + error.message);
    console.log("Error code - " + error.code);
}